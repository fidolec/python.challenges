# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 18:51:24 2022

@author: fidolec
"""

#------------------------------------------------------------------------------

cats = {}

for i in range(1, 101):
    cats["cat"+str(i)] = "no hat"
    
a = 1
while a <= 100:
    for i in range(a, len(cats)+1, a):
        if cats["cat"+str(i)] == "no hat":
            cats["cat"+str(i)] = "hat"
        else:
            cats["cat"+str(i)] = "no hat"
    a += 1

print()
print("==========================================")
for i in range(1, len(cats)+1):
    if cats["cat"+str(i)] == "hat":
        print(f"A cat number {i} has a hat")

print("==========================================")
#------------------------------------------------------------------------------

