# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 10:34:39 2022

@author: fidolec
"""
#------------------------------------------------------------------------------
from pathlib import Path
import csv

def get_second_element(item):
    return item[1] 
#------------------------------------------------------------------------------

path = Path.home() / "Desktop" / "practice_files"
path1 = path / "scores.csv"
path2 = path / "high_scores.csv"

file1 = path1.open(mode = "r", encoding = "utf-8", newline = "")

with path1.open(mode = "r", encoding = "utf-8", newline = "") as file:
    reader = csv.DictReader(file)
    high_scores = []
    for row in reader:
        high_scores.append([row[reader.fieldnames[0]], row[reader.fieldnames[1]]])

for scores in high_scores:
    scores[1] = int(scores[1])

high_scores.sort(key=get_second_element)
score_dict = dict(high_scores)
score = []

for key, value in score_dict.items():
    score.append(({"names" : key, "score" : value}))
   
file2 = path2.open(mode="w", encoding="utf-8", newline="")
writer = csv.DictWriter(file2, fieldnames=["names", "score"])
writer.writeheader()
writer.writerows(score)
file1.close()
file2.close()
#------------------------------------------------------------------------------

