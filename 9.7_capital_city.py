# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 16:09:47 2022

@author: fidolec
"""

#------------------------------------------------------------------------------
import random
#------------------------------------------------------------------------------

capitals_dict = {
                'Alabama': 'Montgomery',
                'Alaska': 'Juneau',
                'Arizona': 'Phoenix',
                'Arkansas': 'Little Rock',
                'California': 'Sacramento',
                'Colorado': 'Denver',
                'Connecticut': 'Hartford',
                'Delaware': 'Dover',
                'Florida': 'Tallahassee',
                'Georgia': 'Atlanta',
                }

state = random.choice(tuple(capitals_dict))
capital = capitals_dict[state]

#------------------------------------------------------------------------------

while True:
    answer = input(f"What is a capital of {state} ? (enter 'exit' then quit): ")
    if answer.lower() == capital.lower():
        print()
        print("CORRECT!!!")
        break
    elif answer == "exit":
        print()
        print(f"The correct answer is {capital}. Goodbye")
        break
    else:
        print()
        print("Incorrect. Try again.")
        continue

#------------------------------------------------------------------------------

