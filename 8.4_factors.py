# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 11:59:14 2022

@author: fidolec
"""


print()
print("---------------------------------------------",end="")
user_number = int(input("Enter a positive integer: "))
print("---------------------------------------------")
for number in range(1, user_number+1):
    if user_number % number == 0:        
        print(f"{number} is factor of {user_number}")
print("---------------------------------------------")        





