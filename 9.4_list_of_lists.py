# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 10:44:05 2022

@author: fidolec
"""

#------------------------------------------------------------------------------
universities = [
                ['California Institute of Technology', 2175, 37704],
                ['Harvard', 19627, 39849],
                ['Massachusetts Institute of Technology', 10566, 40732],
                ['Princeton', 7802, 37000],
                ['Rice', 5879, 35551],
                ['Stanford', 19535, 40569],
                ['Yale', 11701, 40500]
                ]
#------------------------------------------------------------------------------

def enrollment_stats(universities):
    for univercity in universities:
        students.append(univercity[1])
        tuition.append(univercity[2])

def mean(students):
    return sum(students)/len(students)
    
def median(students):
    students.sort()
    return students[int(len(students)//2) -1 + len(students) % 2]

#------------------------------------------------------------------------------

students = []
tuition = []

#------------------------------------------------------------------------------

enrollment_stats(universities)

print()
print("============================================")
print(f"Total students:     {sum(students):,}")
print(f"Total tuition:      $ {sum(tuition):,}")
print(f"Student mean:       {mean(students):,.2f}")
print(f"Student median:     {median(students):,}")
print(f"Tuition mean:       $ {mean(tuition):,.2f}")
print(f"Tuition median:     $ {median(tuition):,}")
print("============================================")

#------------------------------------------------------------------------------
