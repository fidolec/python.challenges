# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 12:11:03 2022

@author: fidolec
"""

import random

#------------------------------------------------------------------------------

def random_element(number, element):
    list = []
    for i in range(number):
        list.append(random.choice(element))
    return list

#------------------------------------------------------------------------------


nouns = ["fossil", "horse", "aardvark", "judge", "chef", "mango",
         "extrovert", "gorilla"]
verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
         "explodes", "curdles"]
adjectives = ["furry", "balding", "incredulous", "fragrant",
              "exuberant", "glistening"]
prepositions = ["against", "after", "into", "beneath", "upon",
                "for", "in", "like", "over", "within"]
adverbs = ["curiously", "extravagantly", "tantalizingly",
           "furiously", "sensuously"]

#------------------------------------------------------------------------------

three_nouns = random_element(3, nouns)
three_verbs = random_element(3, verbs)
three_adjectives = random_element(3, adjectives)
two_prepositions = random_element(2, prepositions)
one_adverbs = random_element(1, prepositions)
a_an = []

if three_adjectives[0][0] in ["a","e", "i", "o", "u"]:
    a_an = ["An"]
else:
    a_an = ["A"]
    
print() 
print("-----------------------------------------------------------------")
print()
print(f"{a_an[0]} {three_adjectives[0]} {three_nouns[0]}")
print()
print()
print(f"{a_an[0]} {three_adjectives[0]} {three_nouns[0]} {three_verbs[0]}\
 {two_prepositions[0]} the {three_adjectives[1]} {three_nouns[2]}")
print()
print(f"{one_adverbs[0]}, the {three_nouns[0]} {three_verbs[1]}")
print()
print(f"the {three_nouns[1]} {three_verbs[2]} {two_prepositions[1]}\
 a {three_adjectives[2]} {three_nouns[2]}")
print()
print("-----------------------------------------------------------------")
#------------------------------------------------------------------------------
