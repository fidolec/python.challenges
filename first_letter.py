# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 15:14:56 2022

@author: TomF
"""
#     Book:     Python Basics: A Practical Introduction to Python 3
#
#                 4.5 Challenge: Pick Apart Your User’s Input

print()
print("___________________________________________________________________")
print("-------------------------------------------------------------------")

first_letter = input("Tell me your password: ")
print()
print(f"The first letter you entered was: {first_letter[0].upper()}")

print()
print("___________________________________________________________________")
print("-------------------------------------------------------------------")

