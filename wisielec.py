# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 21:37:08 2022

@author: fidolec
"""

import random


passwords = ["kotek", "kasia", "tomek", "balon", "rower"]

password = passwords[random.randrange(0,len(passwords),1)]
password_mod = " ".join(password)
password_list = password_mod.split()

hangman_view = [(" "*60,"_"*15), 
                 (" "*60, "|"," "*6,"|"," "*6,"|"),
                 (" "*60, "|"," "*6,"O"," "*6,"|"),
                 (" "*60, "|"," "*6,"|"," "*6,"|"),
                 (" "*60, "|"," "*5,"/","|","\\"," "*5,"|"),
                 (" "*60, "|", " "*4, "/"," ",  "|", " ", "\\", " "*4, "|"),
                 (" "*60, "|"," "*6,"|"," "*6,"|"),
                 (" "*60, "|", " "*5, "/", " ", "\\", " "*5, "|"),
                 (" "*60, "|", " "*4, "/"," ",  " ", " ", "\\", " "*4, "|"),
                 (" "*60, "|", " "*3, "/"," "*2,  " ", " "*2, "\\", " "*3, "|"),
                 (" "*60, "|"," "*6," "," "*6,"|"),
                 (" "*59, "/", " ", "\\"," "*11, "/", " ", "\\"),
]

user_answer = ["_" for i in range(len(password_list))]
bad_answers = 0


def draw_hangman(bad_answers):
    
    """This function draw a hangman after a bad answer"""
    
    for bad_answer in range(bad_answers):
        print()
        for item in range(len(hangman_view[bad_answer])):
            print(hangman_view[bad_answer][item],end="")
    print()
    print(f"Literki hasła {' '.join(user_answer)}")
    

def check_operations():
    
    """ Operating with lists and strings """
    
    print("Brawo!!! Zgadłeś")
    for num in range(len(password_list)):
        if letter in password_list[num]:
            index = password_list.index(letter)
            password_list[index] = "_"
            user_answer[index] = letter
        else:
            continue
    print(f"Literki hasła {' '.join(user_answer)}")



while "".join(user_answer) != password:
    if bad_answers < 12:    
        letter = input("Podaj literke do hasła: ")
        if letter in password_list:
            check_operations()
        else:
            bad_answers += 1
            draw_hangman(bad_answers)
    else:
        break
    
if bad_answers < 12 or "".join(user_answer) == password:
    print("GRATULACJE!!! YOU WIN!!!")    
else:
    print()
    print("Przykro mi ale przegrałeś")

     