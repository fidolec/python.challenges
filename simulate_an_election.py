# -*- coding: utf-8 -*-
"""
Created on Wed Feb  9 14:43:14 2022

@author: fidolec
"""
#-----------------------------------------------------------------------------

import random

#-----------------------------------------------------------------------------

def simulate_election(chance_of_win):
    if random.random() < chance_of_win:
        return "Candidate_A"
    else: 
        return "Candidate_B"

def simulate(chance_of_win):
    win_A = 0
    win_B = 0
    for simulate in range(10_000):
        if simulate_election(chance_of_win) == "Candidate_A":
            win_A += 1
        else:
            win_B += 1
    if win_A > win_B:
        return "Candidate_A"
    else:
        return "Candidate_B"
    
#-----------------------------------------------------------------------------
    
winning_chance = [0.87, 0.65, 0.17]

win_in_reg = [simulate(winning_chance[i]) for i in range(len(winning_chance))]

if win_in_reg.count("Candidate_A") > win_in_reg.count("Candidate_B"):
    print()
    print("And the winner is... Candidate A")
else:
    print()
    print("And the winner is... Candidate B")
    
#-----------------------------------------------------------------------------
