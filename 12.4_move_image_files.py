# -*- coding: utf-8 -*-
"""
Created on Sun Feb 13 13:21:30 2022

@author: fidolec
"""
#------------------------------------------------------------------------------
from pathlib import Path
#------------------------------------------------------------------------------

path = Path.home() / "Desktop" / "practice_files"
new_folder = path / "images"
new_folder.mkdir(exist_ok=True)
files_type = [".jpg", ".png", ".gif"]

#------------------------------------------------------------------------------

print()
num_files = 0    
for file_type in path.rglob("*"):
    if file_type.suffix in files_type:
        file_type.replace(new_folder / file_type.name)
        print(f"Removing {file_type.name}.........")
        num_files += 1
print()
print(f"{num_files} files removed to {new_folder}")    
    
#------------------------------------------------------------------------------    
    




