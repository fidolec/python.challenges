# -*- coding: utf-8 -*-
"""
Created on Sat Feb 12 09:21:32 2022

@author: fidolec
"""
#------------------------------------------------------------------------------

class Animal:
    
    legs = 4
    
    def __init__(self, breed, name, age):
        self.breed = breed
        self.name = name
        self.age = age
        
    def __str__(self):
        return (f"Hi, my name is {self.name} and I'm a {self.breed}."
                + " " + f"I have {Animal.legs} legs")

    def walk(self):
        return "I'm walking"
    
    def eat(self):
        return "I'm hungry and i like eat grass and hay"

class Cow(Animal):
    pass

class Pig(Animal):
    def eat(self):
        return "I'm always hungry and i like eat EVERYTHING!"

class Horse(Animal):
    pass

#------------------------------------------------------------------------------

lucy = Cow("cow", "Lucy", 10)
amanda = Pig("pig", "Amanda", 2)
harold = Horse("horse", "Harold", 6)

print()
print(lucy)
print(amanda.eat())
print(harold.name)
print(harold.walk())
print(amanda.breed)
print(lucy.name, lucy.age)
print(harold.eat())
print()

#------------------------------------------------------------------------------
