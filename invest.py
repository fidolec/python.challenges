# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 11:44:55 2022

@author: fidolec
"""

def invest(amount, rate, years):
    
    """The function calculates the value of your investment 
    from the initial amount at a specified time(in years) 
    and the specified interest rate"""
    
    for year in range(years):
        amount += amount * rate/100
        print(f"year {year+1}: $ {amount:,.2f}")

amount = float(input("Enter initial amount: "))
years = int(input("Enter the number of years of investment: "))
rate = float(input("Enter the investment interest rate in %: "))

print()
print("**********************************************")
invest(amount, rate, years)
print("**********************************************")

