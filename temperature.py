# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 10:00:43 2022

@author: fidolec
"""

def convert_cel_to_far(celsius_temp):
    """Convert a temperature in Celsius to temperature in Fahrenheit"""
    fahrenheit_temp = celsius_temp * 9/5 + 32
    print(f"{celsius_temp} degrees C = {fahrenheit_temp:.2f} degrees F")


def convert_far_to_cel(fahrenheit_temp):
    """Convert a temperature in Fahrenheit to temperature in Celsius"""
    celsius_temp = (fahrenheit_temp - 32) * 5/9
    print(f"{fahrenheit_temp} degrees F = {celsius_temp:.2f} degrees C")


print()
print("----------------Temperature converter---------------")

fahrenheit_temp = float(input("Enter a temperature in degrees F: "))

celsius_temp = float(input("Enter a temperature in degrees C: "))

print()
print("____________________________________________________")
print("____________________________________________________")
print()

convert_far_to_cel(fahrenheit_temp)
print()
convert_cel_to_far(celsius_temp)

print()
print("----------------------------------------------------")
