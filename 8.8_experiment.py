# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 14:21:44 2022

@author: fidolec
"""
#-----------------------------------------------------------------------------
import random

def coin_flip():
    """Randomly return 'heads' or 'tails'."""
    if random.randint(0, 1) == 0:
        return "heads"
    else:
        return "tails"
#-----------------------------------------------------------------------------
tails = 0
heads = 0
every = 0
#-----------------------------------------------------------------------------
for i in range(10_001):
    while tails == 0 or heads == 0:
        if coin_flip() == "heads":
            heads += 1
        else:
            tails += 1
    every = every + heads + tails
    heads = 0
    tails = 0

print()
print(f"The average number of flips per trial: {round(every/10_000)}")

#-----------------------------------------------------------------------------
