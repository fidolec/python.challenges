# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 18:56:45 2022

@author: fidolec
"""
print()
print("-------------------------------------------------------------")

base = int(input("Enter a base: "))

exponent = int(input("Enter an exponent: "))
print()

print(f"{base} to the power of {exponent} = {base ** exponent}")

print()
print("-------------------------------------------------------------")

