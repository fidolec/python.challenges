# -*- coding: utf-8 -*-
"""
Created on Sun Feb  6 17:03:48 2022

@author: fidolec
"""

#     Book:     Python Basics: A Practical Introduction to Python 3
#
#                 4.9 Challenge: Turn Your User Into a L33t H4x0r

print()
print("___________________________________________________________________")
print("-------------------------------------------------------------------")

translate = input("Enter some text: ")
print()
print(translate.replace("a", "4")
               .replace("b", "8")
               .replace("e", "3")
               .replace("l", "1")
               .replace("o", "0")
               .replace("s", "5")
               .replace("t", "7"))
print()
print("___________________________________________________________________")
print("-------------------------------------------------------------------")